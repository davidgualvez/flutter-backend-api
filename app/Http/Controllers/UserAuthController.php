<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserAuthController extends Controller
{
    //
    public function login(Request $request){
        // return $request->all();

        $user = User::where('email', $request->email)->first();

        if(!$user){
            return response()->json([
                'message' => 'Email not found'
            ], 400);
        }

        if(!Hash::check($request->password, $user->password)){
            return response()->json([
                'message' => 'Incorrect Password'
            ], 400);
        }

        // revoke all tokens for flutter
        $user->tokens()->where('name', 'flutter')->delete();
        $token = $user->createToken('flutter');

        return response()->json([
            'message' => 'ok',
            'data' => [
                'token' => $token->plainTextToken
            ]
        ]);
    }

    public function register(Request $request){

        $u = User::where('email', $request->email)->first();
        if($u){
            return response()->json([
                'message' => 'Email already exists'
            ], 400);
        }

        $user = User::create(
            array_merge(
                $request->all(),
                [
                    'password' => Hash::make($request->password)
                ]
            )
        );

        $token = $user->createToken('flutter');

        return response()->json([
            'message' => 'ok',
            'data' => [
                'token' => $token->plainTextToken
            ]
        ]);
    }
}
